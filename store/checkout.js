export const state = () => ({
  stepCounts: 2,
})

export const mutations = {
  nextStep(state) {
    state.stepCounts += 1
  },
  goTo(state, index) {
    state.stepCounts = index
  }
}

export const getters = {
  getStepCounts: ({ stepCounts }) => stepCounts,
}
