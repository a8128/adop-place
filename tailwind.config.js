module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        '128': '60rem',
        'w34' : 'w-3/4'
      },
      colors: {
        "blog1": "#7D6A7D",
        'groon': '#CEF1EE',
        
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  mode: "jit"
}
